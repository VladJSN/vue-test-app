import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import { objectToURIParams } from '../helper';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    filters: {
      status: '',
      gender: '',
      species: '',
      name: ''
    },
    cards: [],
  },
  getters: {
    cards (state) {
      return state.cards;
    },
    filters (state) {
      return state.filters;
    }
  },
  mutations: {
    gotCards (state, cards) {
      state.cards = [...cards];
    },
    setFilters (state, filters) {
      state.filters = {...filters};
    }
  },
  actions: {
    async getCards({commit, getters}, payload) {
      const {data} = await axios.get('https://rickandmortyapi.com/api/character/' + objectToURIParams(getters.filters));

      return commit('gotCards', data && data.results ? data.results : []);
    },
  },
  modules: {
  }
});
