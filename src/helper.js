const objectToURIParams = (sourceObj) => {
	let filteredObject = {}, key, params = '';
	const cleanObject = {...sourceObj};

	for (key in cleanObject) {
		if (cleanObject.hasOwnProperty(key) && cleanObject[key] !== '') {
			filteredObject[key] = cleanObject[key];
		}
	}

	if (Object.keys(filteredObject).length > 0) {
		params = '?' + Object.keys(filteredObject).map(function(k) {
			return encodeURIComponent(k) + "=" + encodeURIComponent(filteredObject[k]);
		}).join('&');
	}

	return params;
};

export {
	objectToURIParams
}
